$(function() {

    $('.el-dropdown').each(function () {

        var choicesBox = new Choices($(this)[0], {
            callbackOnCreateTemplates: function (template) {
                var classNames = this.config.classNames;
                return {
                    item: (data) => {
                        return template(`
<div class="${classNames.item}
    ${data.highlighted ? classNames.highlightedState : classNames.itemSelectable}"
    data-item
    data-id="${data.id}"
    data-value="${data.value}"
    ${data.active ? 'aria-selected="true"' : ''}
    ${data.disabled ? 'aria-disabled="true"' : ''}
>
    <i class="${data.label} fa-3x" style="color:${this.element.dataset.color}"></i> ${data.value}
</div>
          `);
                    },
                    choice: (data) => {
                        return template(`
<div class="${classNames.item} ${classNames.itemChoice}
    ${data.disabled ? classNames.itemDisabled : classNames.itemSelectable}"
    data-choice
    ${data.disabled ? 'data-choice-disabled aria-disabled="true"' : 'data-choice-selectable'}
    data-id="${data.id}"
    data-value="${data.value}"
    ${data.groupId > 0 ? 'role="treeitem"' : 'role="option"'}
>
    <i class="${data.label} fa-3x" style="color:${this.element.dataset.color}"></i> ${data.value}
</div>
          `);
                    },
                };
            }
        });
    });
});
