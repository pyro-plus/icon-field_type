<?php

return [
    'color' => [
        'type'     => 'anomaly.field_type.colorpicker',
        'required' => true,
        'config'   => [
            'default_value' => '#000',
        ],
    ],
];
