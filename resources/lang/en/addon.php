<?php

return [
    'title'       => 'Icon',
    'name'        => 'Icon Field Type',
    'description' => 'Field type for show icon',
];
