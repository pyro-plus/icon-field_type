<?php namespace Defr\IconFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldType;
use Defr\IconFieldType\Command\BuildOptions;

/**
 * Class IconFieldType
 *
 * @package       defr.field_type.icon
 *
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class IconFieldType extends FieldType
{

    /**
     * The input view.
     *
     * @var null|string
     */
    protected $inputView = null;

    /**
     * The field type config.
     *
     * @var array
     */
    protected $config = [
        'mode'    => 'dropdown',
        'handler' => 'Defr\IconFieldType\IconFieldTypeOptions@handle',
    ];

    /**
     * The dropdown options.
     *
     * @var null|array
     */
    protected $options = null;

    /**
     * Get the dropdown options.
     *
     * @return array
     */
    public function getOptions()
    {
        if ($this->options === null)
        {
            $this->dispatch(new BuildOptions($this));
        }

        return $this->options;
    }

    /**
     * Set the options.
     *
     * @param  array   $options
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get the placeholder.
     *
     * @return null|string
     */
    public function getPlaceholder()
    {
        if (!$this->placeholder && !$this->isRequired())
        {
            return 'defr.field_type.icon::input.placeholder';
        }

        return $this->placeholder;
    }

    /**
     * Return the input view.
     *
     * @return string
     */
    public function getInputView()
    {
        if ($view = parent::getInputView())
        {
            return $view;
        }

        return 'defr.field_type.icon::' . $this->config('mode', 'dropdown');
    }

    /**
     * Get the class.
     *
     * @return null|string
     */
    public function getClass()
    {
        if ($class = parent::getClass())
        {
            return $class;
        }

        return $this->config('mode') == 'dropdown' ? 'custom-select form-control' : 'c-inputs-stacked';
    }
}
