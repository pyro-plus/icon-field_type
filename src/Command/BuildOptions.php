<?php namespace Defr\IconFieldType\Command;

use Defr\IconFieldType\IconFieldType;
use Illuminate\Container\Container;

/**
 * Class BuildOptions
 *
 * @package       defr.field_type.icon
 *
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class BuildOptions
{

    /**
     * The field type instance.
     *
     * @var IconFieldType
     */
    protected $fieldType;

    /**
     * Create a new BuildOptions instance.
     *
     * @param IconFieldType $fieldType
     */
    function __construct(IconFieldType $fieldType)
    {
        $this->fieldType = $fieldType;
    }

    /**
     * Handle the command.
     *
     * @param Container $container
     */
    public function handle(Container $container)
    {
        $handler = array_get($this->fieldType->getConfig(), 'handler');

        if (!class_exists($handler) && !str_contains($handler, '@'))
        {
            $handler = array_get($this->fieldType->getHandlers(), $handler);
        }

        if (is_string($handler) && !str_contains($handler, '@'))
        {
            $handler .= '@handle';
        }

        $container->call($handler, ['fieldType' => $this->fieldType]);
    }
}
