<?php namespace Defr\IconFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypePresenter;

/**
 * Class IconFieldTypeOptions
 *
 * @package       defr.field_type.icon
 *
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class IconFieldTypePresenter extends FieldTypePresenter
{

    /**
     * Return the selection key.
     *
     * @return string|null
     */
    public function key()
    {
        return $this->object->getValue();
    }

    /**
     * Return the selection value.
     *
     * @return string|null
     */
    public function value()
    {
        $options = $this->object->getOptions();

        if (($key = $this->key()) === null)
        {
            return null;
        }

        if (!str_contains($value = array_get($options, $key), '::'))
        {
            return $value;
        }

        return trans($value);
    }

    /**
     * Return the contextual human value.
     *
     * @return null|string
     */
    public function __print()
    {
        return $this->value();
    }
}
