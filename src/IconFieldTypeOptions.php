<?php namespace Defr\IconFieldType;

use Anomaly\Streams\Platform\Ui\Icon\IconRegistry;

/**
 * Class IconFieldTypeOptions
 *
 * @package       defr.field_type.icon
 *
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class IconFieldTypeOptions
{

    /**
     * Handle the FT options.
     *
     * @param  IconFieldType $fieldType
     * @param  IconRegistry  $icons       The icons
     * @return array
     */
    public function handle(IconFieldType $fieldType, IconRegistry $icons)
    {
        $options = array_get($fieldType->getConfig(), 'options', []);
        $icons   = $icons->getIcons();

        $fieldType->setOptions((!count($options)) ? $icons : array_combine(
            $options,
            array_map(
                function ($key) use ($icons)
                {
                    return $icons->get($key);
                },
                $options
            )
        ));
    }
}
