# Icon Field Type
## Streams Platform Addon. `icon-field_type` for PyroCMS.
Select icon from the list - field type.
## Features
- Use only native Pyro icons registry class
- Easy to install
- Much easier to use
***

## Installation
Just copy addon folder to `addons/${appReference}/defr`
> Do not forget to clear cache after that
```bash
$ php artisan assets:clear
```
***

## Usage
- You should see the new one type **Icon** in the list of field types
![field_type.png](https://bitbucket.org/repo/7EzBp5K/images/1329575570-field_type.png)
- Use it
![field.png](https://bitbucket.org/repo/7EzBp5K/images/2295907712-field.png)
![list.png](https://bitbucket.org/repo/7EzBp5K/images/3105638205-list.png)
***
